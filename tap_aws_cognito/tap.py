"""AwsCognito tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_aws_cognito.streams import UsersStream

STREAM_TYPES = [
    UsersStream
]


class TapAwsCognito(Tap):
    """AwsCognito tap class."""
    name = "tap-aws-cognito"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_key",
            th.StringType,
            required=True,
            description="The access_key to authenticate against the service"
        ),
        th.Property(
            "secret_key",
            th.StringType,
            required=True,
            description="The secret_key to authenticate against the service"
        ),
        th.Property(
            "pool_id",
            th.StringType,
            default="us-east-1_R0Qjtv1of",
            description="The url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == '__main__':
    TapAwsCognito.cli()
